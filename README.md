# PhD-thesis-template

Nice template for PhD thesis in LaTex. Use with your favourite text-editor. [I recommend Sublime and LaTeXTools](https://latextools.readthedocs.io/)

See thesis.pdf for a compiled version of this template.

## Contributing
Feel free to fork this project and make your own improvements.

## Authors and acknowledgment
Thanks to Jonas Kjellstadli for sending me an earlier version of this template

## License
MIT Licence 
